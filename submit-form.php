<!DOCTYPE html>
    <html lang="fr">
        <head>
            <title>OSCAR École européenne d'ostéopathie</title>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0">
            <meta name="robots" content="INDEX,FOLLOW" />
            <meta name="theme-color" content="#ffffff">

            <!-- SEO  -->
            <meta name="description" content="OSCAR, clinique dispensés par nos étudiants encadrés par des enseignants ostéopathes professionnels." />
            <meta name="author" content="Passeri Mario" />
            <meta name="copyright" content="Passeri Mario" />
            <meta name="geo.placename" content="Strasbourg, Alsace">
            <meta name="geo.country" content="FR">

            <!-- Informations réseaux sociaux -->
            <meta property="og:title" content="OSCAR École européenne d'ostéopathie"/>
            <meta property="og:type" content="website" />
            <meta property="og:url" content="http://dwca.mariopasseri.eu/homework/"/>
            <meta property="og:image" content="http://dwca.mariopasseri.eu/homework/assets/img/logoCliniqueOscar.jpg"/>
            <meta property="og:description" content="OSCAR, clinique dispensés par nos étudiants encadrés par des enseignants ostéopathes professionnels."/>

            <!-- CSS -->
            <link rel="stylesheet" type="text/css" href="assets/css/normalize.css">
            <link rel="stylesheet" type="text/css" href="assets/css/style.css">
        </head>

    <body>

    <!-- header -->

        <header>
            <div class="flex-header">
                <img class="logo_oscar" src="assets/img/logoCliniqueOscar.jpg" alt="">
                <div>
                    <p class="banniere1">Certification</p>
                    <img class="logo_ministere" src="assets/img/sante.jpg" alt="">
                    <p class="banniere2">Prenez rendre-vous</p>
                </div>
            </div>

            <div class="zone-reservation">
                <h1 class="reservation">Reservez une consultation ostéopatique</h1>
            </div>
        </header>

    <!-- main -->

        <main>

            <?php

                echo "<p> Nom : " . htmlentities($_POST["nom"]) . "</p>";

                echo "<p>Prénom : " . htmlentities($_POST["prenom"]) . "</p>";

                echo "<p>Téléphone : " . htmlentities($_POST["telephone"]) . "<p/>";

                echo "<p> Email: " . htmlentities($_POST["email"]) . "</p>";

                // Conversion de la date du formulaire
                $date_Formulaire = date("d/m/Y", strtotime($_POST["date"]));

                echo "<p>Réservation : " . $date_Formulaire . "</p>";

                echo "<p>Heure : " . htmlentities($_POST["heure"]) . "</p>";

                echo "<p>Tarif : " . htmlentities($_POST["tarif"]) . " € <p>";

                // Si la case est coché

                if(isset($_POST["autorisation"])){
                    echo "<p> Autorisation : " . htmlentities($_POST["autorisation"]) . "</p>";
                }
                else{
                    echo "<p> Autorisation : off</p>";
                }

            ?>

        </main>

    <!-- footer -->

        <footer>

            <div>
                <img src="assets/img/logoEcoleOscar.jpg" alt="">
            </div>
            <div class="text-footer">
                <h2>École Oscar</h2>
                <p>Campus Privé d'Alsace</p>
                <p>24a Rue des magasin - 67000 Strasbourg</p>
                <p>Tel: +33 (0)3 88 23 14 14 - info@ecoleoscar.com</p>
            </div>

        </footer>

    </body>
</html>