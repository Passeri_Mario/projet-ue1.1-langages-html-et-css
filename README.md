Lien du projet : 

#### http://dwca.mariopasseri.eu/homework/

Description : 

Projet UE1.1 Langages HTML et CSS
Christoffel Eric – LP DWCA 2020
Description
Une clinique Ostéopathique, rattachée à une école privée d’Ostéopathie, souhaite un site web pour la
prise de Rendez-Vous en ligne.
Le site sera Responsive, une version Desktop (800px en largeur), une version Mobile (400px en largeur).
Les maquettes sont présentées en annexe. Les ressources images sont également fournies, et devront être
utilisées sans retouche.
Vous avez une totale liberté dans les techniques utilisées pour le développement:

Design du site : CSS Grid, CSS Flexbox, Framework Bootstrap, ... totale liberté
Respectez les dimensions : Desktop de 800px de largeur, Mobile de 400px de large, utilisation de
media-queries
Les images sont utilisées sans retouche logiciel
Sites accessible depuis un serveur distant (Alwaysdata, ou autre)
Contraintes de contenus :

Tous les champs de formulaire sont obligatoires
Certains champs de formulaire requiert un format de saisie précis
Date et heure de RDV : en HTML5, ou via un widget Javascript, au choix. Les réservations sont du
lundi au vendredi, de 9h à 17h, la durée des consultations étant d’une heure. Vérifiez que votre
solution fonctionne principalement sur Chrome, Firefox, Edge (Windows), Safari (MacOS), desktop
et mobile. Si votre solution ne fonctionne pas pour un navigateur donné, la date et/ou l’heure,
pourront être saisies manuellement en respectant le format 18/11/2020 à 09:00, par ex.
D’autres solutions plus ergonomiques peuvent être envisagées pour saisir la date et/ou l’heure de
RDV (CSS, Javascript, widget, ..) (User eXperience improvment).
Résultat attendu
Un site web distant avec cette unique page, le formulaire ne sera pas traité, hormis l’affichage des données
transmises du formulaire via la variable $_POST, et la fonction PHP print_r(). Le site responsive sera testé
avec l’outil choix du dispositif des navigateurs (Device Toolbar) à l’aide des outils de Développement dédié
à la rétro-ingénierie.